import express from 'express';

const router = express.Router();

router.post('/api/users/signout', (req, res) => {
	res.send('User has signed out!');
});

export { router as signOutRouter };
